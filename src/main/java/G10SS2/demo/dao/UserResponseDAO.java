package G10SS2.demo.dao;

import G10SS2.demo.entity.UserResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserResponseDAO extends JpaRepository<UserResponse,Integer> {

}
