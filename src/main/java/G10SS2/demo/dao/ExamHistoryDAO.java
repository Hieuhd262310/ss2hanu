package G10SS2.demo.dao;

import G10SS2.demo.entity.ExamHistory;
import G10SS2.demo.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExamHistoryDAO extends JpaRepository<ExamHistory, Integer> {
    List<ExamHistory> findByUser(User user);
}
