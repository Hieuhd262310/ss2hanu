package G10SS2.demo.dao;

import G10SS2.demo.entity.Exam;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExamDAO extends JpaRepository<Exam, Integer> {

}
