package G10SS2.demo.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Entity
@Data
@Table(name = "exam")
public class Exam {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "duration")
    private int duration;

    @Column(name = "exam_type")
    private String examType;

    @Column(name = "paragraph1" ,columnDefinition = "TEXT")
    private String paragraph1;

    @Column(name = "paragraph2" ,columnDefinition = "TEXT")
    private String paragraph2;

    @Column(name = "paragraph3" ,columnDefinition = "TEXT")
    private String paragraph3;

    @Column(name = "paragraph4" ,columnDefinition = "TEXT")
    private String paragraph4;

    @Column(name = "paragraph5" ,columnDefinition = "TEXT")
    private String paragraph5;

    @Column(name = "paragraph6" ,columnDefinition = "TEXT")
    private String paragraph6;

    @Column(name = "paragraph7",columnDefinition = "TEXT")
    private String paragraph7;

    @OneToMany(mappedBy = "exam", fetch = FetchType.LAZY,cascade = { CascadeType.DETACH, CascadeType.PERSIST,
        CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE})
    private List<Question> questions;

    @OneToMany(mappedBy = "exam", fetch = FetchType.LAZY,cascade = { CascadeType.DETACH, CascadeType.PERSIST,
            CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE})
    private List<ExamHistory> examHistories;

}
