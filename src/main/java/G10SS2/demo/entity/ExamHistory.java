package G10SS2.demo.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "exam_history")
public class ExamHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;


@ManyToOne(cascade = { CascadeType.DETACH, CascadeType.PERSIST,
        CascadeType.MERGE, CascadeType.REFRESH})
@JoinColumn(name = "exam_id")
    private Exam exam;

    @Column(name = "score")
    private int score;

    @Column(name = "date_taken")
    private Date dateTaken;

    @OneToMany(mappedBy = "examHistory", fetch = FetchType.LAZY,cascade = { CascadeType.DETACH, CascadeType.PERSIST,
            CascadeType.MERGE, CascadeType.REFRESH},
            orphanRemoval = true)
    private List<UserResponse> userResponses;
}