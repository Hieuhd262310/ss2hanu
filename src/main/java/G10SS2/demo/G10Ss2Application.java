package G10SS2.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class G10Ss2Application {

	public static void main(String[] args) {
		SpringApplication.run(G10Ss2Application.class, args);
	}

}
