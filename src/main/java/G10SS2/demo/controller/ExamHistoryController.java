package G10SS2.demo.controller;

import G10SS2.demo.entity.ExamHistory;
import G10SS2.demo.entity.User;
import G10SS2.demo.service.ExamHistoryService;
import G10SS2.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Controller
public class ExamHistoryController {
    @Autowired
    private ExamHistoryService examHistoryService;
    @Autowired
    private UserService userService;


    @GetMapping("/exams/history")
  //  public String history(@AuthenticationPrincipal UserDetails userDetails, Model model) {
    public String history( Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        OAuth2User oAuth2User = (OAuth2User) authentication.getPrincipal();
        String name = oAuth2User.getAttribute("name");
        String email = oAuth2User.getAttribute("email");

        User user = userService.findByEmail(email);
        List<ExamHistory> examHistories = examHistoryService.findByUser(user);
        model.addAttribute("userExamResults", examHistories);
        return "history";
    }
    @GetMapping("/exams/history/{id}")
    public  String getExamHistoryDetail(@PathVariable("id") int id, Model model){
        ExamHistory examHistory = examHistoryService.getUserExamResultById(id);
        model.addAttribute("examHistory", examHistory);
        return "histoty_detail";

    }


}
