//package G10SS2.demo.controller;
//
//import G10SS2.demo.entity.Question;
//import G10SS2.demo.service.QuestionService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.servlet.mvc.support.RedirectAttributes;
//
//import java.util.List;
//
//@Controller
//public class QuestionController {
//    private QuestionService questionService;
//
//    @Autowired
//    public QuestionController(QuestionService questionService) {
//        this.questionService = questionService;
//    }
//
//    @GetMapping("/login")
//    public String login() {
//        return "question/login";
//    }
//
//    @GetMapping("/allQuestions")
//    public String allQuestions(Model model) {
//        List<Question> questions = questionService.getAllQuestion();
//        model.addAttribute("questions", questions);
//        return "question/questions";
//    }
//    @GetMapping("/questions/new")
//    public String newQuestionForm(Model model) {
//        Question question = new Question();
//        model.addAttribute("question", question);
//        return "question/new_question";
//    }
//    @PostMapping("/questions")
//    public String saveQuestion(@ModelAttribute("question") Question question) {
//        questionService.saveQuestion(question);
//        return "redirect:/allQuestions";
//    }
//    @PostMapping("/delete/{id}")
//    public String deleteQuestion(@PathVariable int id, RedirectAttributes redirectAttributes) {
//        Question existQuestion = questionService.getQuestionById(id);
//        if (existQuestion != null) {
//            questionService.deleteQuestion(id);
//            redirectAttributes.addFlashAttribute("successMessage", "Question successfully deleted.");
//            return "redirect:/allQuestions";
//        } else {
//            redirectAttributes.addFlashAttribute("errorMessage", "Question not found.");
//            return "redirect:/allQuestions";
//        }
//    }
//
//    @GetMapping("/questions/edit/{id}")
//    public String showEditQuestionForm(@PathVariable int id, Model model) {
//        Question question = questionService.getQuestionById(id);
//        if (question != null) {
//            model.addAttribute("question", question);
//            return "question/edit_question";
//        } else {
//            return "redirect:/allQuestions"; // Redirect if the question doesn't exist
//        }
//    }
//
//    @PostMapping("/questions/edit/{id}")
//    public String updateQuestion(@PathVariable int id, @ModelAttribute("question") Question question, Model model) {
//        Question existingQuestion = questionService.getQuestionById(id);
//        if (existingQuestion != null) {
//            existingQuestion.setTitle(question.getTitle());
//            existingQuestion.setOption1(question.getOption1());
//            existingQuestion.setOption2(question.getOption2());
//            existingQuestion.setOption3(question.getOption3());
//            existingQuestion.setOption4(question.getOption4());
//            existingQuestion.setRightAnswer(question.getRightAnswer());
//            questionService.saveQuestion(existingQuestion);
//            return "redirect:/allQuestions";
//        } else {
//            model.addAttribute("errorMessage", "Question not found");
//            return "question/edit_question";
//        }
//    }
//}

package G10SS2.demo.controller;

import G10SS2.demo.entity.Question;
import G10SS2.demo.entity.Exam;
import G10SS2.demo.entity.QuestionWrapper;
import G10SS2.demo.service.QuestionService;
import G10SS2.demo.service.ExamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Optional;

@Controller
public class QuestionController {
    private QuestionService questionService;
    private ExamService examService;

    @Autowired
    public QuestionController(QuestionService questionService, ExamService examService) {
        this.questionService = questionService;
        this.examService = examService;
    }

    @GetMapping("/exams/{id}/questions")
    public String allQuestions(@PathVariable int id, Model model) {
        // lấy kỳ thi bằng ID
        Optional<Exam> exam = examService.getExamById(id);
        return exam.map(e -> {
            // Nếu kỳ thi tồn tại, lấy ra các câu hỏi của kỳ thi đó
            model.addAttribute("questions", e.getQuestions());

            // thêm id của exam khi render ra danh sách quiz
            model.addAttribute("examId", id);
            return "question/questions";
        }).orElse("redirect:/exams");  // Nếu không tìm thấy kỳ thi, redirect người dùng trở lại danh sách các kỳ thi
    }


    @GetMapping("/questions/{examId}/new")
    public String newQuestionForm(@PathVariable int examId, Model model) {

        Optional<Exam> optionalExam = examService.getExamById(examId);
        if (optionalExam.isPresent()) {
            Question question = new Question();
            model.addAttribute("question", question);
            model.addAttribute("examId", examId);
            return "question/new_question";
        } else {
            // Nếu không tìm thấy kỳ thi, có thể trả về một trang lỗi hoặc redirect về trang danh sách các kỳ thi
            model.addAttribute("errorMessage", "Exam with ID " + examId + " does not exist.");
            return "redirect:/exams"; // Chuyển hướng người dùng trở lại danh sách các kỳ thi
        }
    }

    @PostMapping("/questions/{examId}")
    public String saveQuestion(@PathVariable int examId, @ModelAttribute("question") Question question) {
        // Lấy thông tin kỳ thi từ ID.
        Optional<Exam> examOptional = examService.getExamById(examId);

        // Kiểm tra xem kỳ thi có tồn tại không
        if (examOptional.isPresent()) {
            Exam exam = examOptional.get();
            // Thiết lập kỳ thi cho câu hỏi và lưu câu hỏi
            question.setExam(exam);
            questionService.saveQuestion(question);
            return "redirect:/exams/{examId}/questions";
        } else {
            return "redirect:/exams";
        }
    }

    @GetMapping("/questions/edit/{id}")
    public String showEditQuestionForm(@PathVariable int id, Model model) {
        // Sử dụng Optional để xử lý trường hợp không tìm thấy câu hỏi
        Optional<Question> questionOptional = Optional.ofNullable(questionService.getQuestionById(id));

        // Kiểm tra xem câu hỏi có tồn tại không và có kỳ thi liên kết không
        if (questionOptional.isPresent() && questionOptional.get().getExam() != null) {
            Question question = questionOptional.get();
            model.addAttribute("question", question);
            model.addAttribute("examId", question.getExam().getId());
            return "question/edit_question";
        } else {
            // Chuyển hướng nếu không có câu hỏi hoặc không có kỳ thi liên kết
            return "redirect:/exams";
        }
    }


    @PostMapping("/questions/edit/{id}")
    public String updateQuestion(@PathVariable int id, @ModelAttribute("question") Question questionDetails) {
        Optional<Question> questionOptional = Optional.ofNullable(questionService.getQuestionById(id));

        if (questionOptional.isPresent()) {
            Question existingQuestion = questionOptional.get();
            // Cập nhật thông tin câu hỏi
            existingQuestion.setTitle(questionDetails.getTitle());
            existingQuestion.setOption1(questionDetails.getOption1());
            existingQuestion.setOption2(questionDetails.getOption2());
            existingQuestion.setOption3(questionDetails.getOption3());
            existingQuestion.setOption4(questionDetails.getOption4());
            existingQuestion.setRightAnswer(questionDetails.getRightAnswer());

            // Lưu các thay đổi vào database
            questionService.saveQuestion(existingQuestion);

            int examId = existingQuestion.getExam().getId();
            return "redirect:/exams/" + examId + "/questions";
        } else {

            return "redirect:/exams";
        }
    }
    @PostMapping("/exam/question/delete/{id}")
    public String deleteQuestion(@PathVariable int id) {
        Question existQuestion = questionService.getQuestionById(id);
        int examId = existQuestion.getExam().getId();
            questionService.deleteQuestion(id);
            return "redirect:/exams/"+examId+"/questions";
    }
}
