package G10SS2.demo.controller;

import G10SS2.demo.entity.*;
import G10SS2.demo.service.ExamService;
import G10SS2.demo.service.UserService;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.*;

@Controller
@RequestMapping("/exams")
public class ExamController {
    @Autowired
    private ExamService examService;
    @Autowired
    private UserService userService;
    @GetMapping()
    public String getAllExam(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        OAuth2User oAuth2User = (OAuth2User) authentication.getPrincipal();
        String name = oAuth2User.getAttribute("name");
        model.addAttribute("exams", examService.getAllExam());
        model.addAttribute("name",name);
        return "exam/listExam";
    }

    @GetMapping("/new")
    public String showNewExamForm(Model model) {
        model.addAttribute("exam", new Exam());
        return "exam/new_exam";
    }

    @PostMapping("/saveOrUpdate")
    public String saveOrUpdateExam(@ModelAttribute("exam") Exam exam) {
        if (exam.getId() != 0) {
            // Update existing exam
            Optional<Exam> existingExamOpt = examService.getExamById(exam.getId());

                Exam existingExam = existingExamOpt.get();
                existingExam.setName(exam.getName());
                existingExam.setDuration(exam.getDuration());
                existingExam.setExamType(exam.getExamType());
                existingExam.setDescription(exam.getDescription());

                // Handle paragraphs based on exam type
                switch (exam.getExamType()) {
                    case "LISTENING":
                        existingExam.setParagraph1(null);
                        existingExam.setParagraph2(null);
                        existingExam.setParagraph3(null);
                        existingExam.setParagraph4(null);
                        existingExam.setParagraph5(null);
                        existingExam.setParagraph6(null);
                        existingExam.setParagraph7(null);
                        break;
                    case "READING":
                        existingExam.setParagraph1(exam.getParagraph1());
                        existingExam.setParagraph2(exam.getParagraph2());
                        existingExam.setParagraph3(exam.getParagraph3());
                        existingExam.setParagraph4(exam.getParagraph4());
                        existingExam.setParagraph5(exam.getParagraph5());
                        break;
                    case "SPEAKING":
                        existingExam.setParagraph1(null);
                        existingExam.setParagraph2(null);
                        existingExam.setParagraph3(null);
                        existingExam.setParagraph4(null);
                        existingExam.setParagraph5(null);
                        existingExam.setParagraph6(exam.getParagraph6());
                        existingExam.setParagraph7(null);
                        break;
                    case "WRITING":
                        existingExam.setParagraph1(null);
                        existingExam.setParagraph2(null);
                        existingExam.setParagraph3(null);
                        existingExam.setParagraph4(null);
                        existingExam.setParagraph5(null);
                        existingExam.setParagraph6(null);
                        existingExam.setParagraph7(exam.getParagraph7());

                        break;
                }
                examService.saveExam(existingExam);
        } else {
            // Save new exam
            examService.saveExam(exam);
        }
        return "redirect:/exams";
    }


    @GetMapping("/edit/{id}")
    public String showEditExamForm(@PathVariable int id, Model model) {
        Optional<Exam> exam = examService.getExamById(id);
        String type = String.valueOf(exam.get().getExamType());
        exam.ifPresentOrElse(
                e -> {
                    model.addAttribute("exam", e);
                    model.addAttribute("questions", e.getQuestions()); // Show questions associated with the exam
                    model.addAttribute("type",type);
                },
                () -> model.addAttribute("error", "Exam not found")
        );
        return "exam/edit_exam";
    }


    @GetMapping("/delete/{id}")
    public String deleteExam(@PathVariable int id) {
        Optional<Exam> exam = examService.getExamById(id);
            examService.deleteExam(id);
        return "redirect:/exams";
    }

    @GetMapping("/take/{id}")
    public String getExamQuestions(@PathVariable int id, Model model) {
        Optional<Exam> examOptional = examService.getExamById(id);
        if (examOptional.isPresent()) {
            Exam exam = examOptional.get();
            List<QuestionWrapper> questionWrapperList = examService.getExamQuestion(id);
            model.addAttribute("questionWrapperList", questionWrapperList);
            model.addAttribute("examDuration", exam.getDuration());
            model.addAttribute("examId", id);

            String examType = exam.getExamType();
            switch (examType) {
                case "READING":
                    model.addAttribute("text1", exam.getParagraph1());
                    model.addAttribute("text2", exam.getParagraph2());
                    model.addAttribute("text3", exam.getParagraph3());
                    model.addAttribute("text4", exam.getParagraph4());
                    model.addAttribute("text5", exam.getParagraph5());
                  //  model.addAttribute("number",0);
                    return "exam/ReadingTest";
                case "LISTENING":
                    model.addAttribute("file", exam.getDescription());
                    return "exam/ListeningTest";
                case "SPEAKING":
                    model.addAttribute("text1", exam.getParagraph6());
                    return "exam/SpeakingTest";
                case "WRITING":
                    model.addAttribute("text1", exam.getParagraph7());
                    return "exam/WritingTest";
                default:
                    System.out.println("Unknown exam type: " + examType);
                    return "redirect:/exams";
            }
        } else {
            return "redirect:/exams";
        }
    }

    @PostMapping("/submit/{id}")
    public String getResult(@PathVariable int id, HttpServletRequest request, Model model, Authentication authentication) {
        List<UserResponse> userResponses = new ArrayList<>();

        // Assume all question IDs are known or retrieve them from the database
        List<Question> questions = examService.getExamQuestionFromData(id);

        for (Question question : questions) {
            String selectedOption = request.getParameter("question" + question.getId());
            if (selectedOption != null) {
                UserResponse response = new UserResponse();
                response.setQuestionId(question.getId());
                response.setSelectedOption(selectedOption);
                userResponses.add(response);
            }
        }

        int score = examService.caculateResult(id, userResponses);
        OAuth2User oAuth2User = (OAuth2User) authentication.getPrincipal();
        String email = oAuth2User.getAttribute("email");
        User user = userService.findByEmail(email);
        if (user == null) {
            user = new User();
            user.setEmail(email);
            user.setUsername(oAuth2User.getAttribute("name"));
            userService.save(user); // Saving user before using in UserExamResult
        }

        ExamHistory result = new ExamHistory();
        result.setUser(user);
        result.setExam(examService.getExamById(id).orElse(null));
        result.setScore(score);
        result.setDateTaken(new Date());
        examService.saveExamResult(result,userResponses);

        model.addAttribute("score", score);
        return "exam/result";
    }



}


