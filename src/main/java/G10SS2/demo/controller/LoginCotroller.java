package G10SS2.demo.controller;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class LoginCotroller {
    @GetMapping("/login")
    public String login() {
        return "question/login";
    }

    @PostMapping("/logout")
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        // Xóa session hiện tại
        request.getSession().invalidate();

        // Xóa cookie JSESSIONID (nếu có)
        // Lưu ý: Nếu bạn sử dụng Spring Security, nó có thể tự động xóa cookie JSESSIONID khi đăng xuất

        // Chuyển hướng người dùng đến trang đăng nhập sau khi đăng xuất thành công
        return "redirect:/login?logout";
    }


}
