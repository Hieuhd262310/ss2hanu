package G10SS2.demo.controller;

import G10SS2.demo.entity.User;
import G10SS2.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/loginSuccess")
    public String loginSuccess(Model model){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        OAuth2User oAuth2User = (OAuth2User) authentication.getPrincipal();

        String name = oAuth2User.getAttribute("name");
        String email = oAuth2User.getAttribute("email");
        String picture = oAuth2User.getAttribute("picture");

        User user = new User();
        user.setUsername(name);
        user.setEmail(email);
        userService.save(user);
        //
        model.addAttribute("name", name);
        model.addAttribute("email", email);
        model.addAttribute("picture", picture);

        return "redirect:/exams";
       // return "dashBoard";
    }
}
