package G10SS2.demo.service;

import G10SS2.demo.dao.ExamDAO;

import G10SS2.demo.dao.ExamHistoryDAO;
import G10SS2.demo.dao.UserResponseDAO;
import G10SS2.demo.entity.*;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ExamService {
    @Autowired
    private ExamDAO examDAO;
    @Autowired
    private ExamHistoryDAO examHistoryDAO;
    @Autowired
    private UserResponseDAO userResponseDAO;

    public Exam saveExam(Exam exam) {
     return  examDAO.save(exam);
       // return exam;
    }

    public Exam saveOExam(Exam exam) {
        return examDAO.save(exam);
    }

    public Optional<Exam> getExamById(int id) {
       return examDAO.findById(id);
    }

    public void deleteExam(int id) {
        examDAO.deleteById(id);
    }

    public List<Exam> getAllExam() {
        return examDAO.findAll();
    }


    public List<QuestionWrapper> getExamQuestion(int id) {
        Optional<Exam> exam = examDAO.findById(id);
        List<Question> questionList = exam.get().getQuestions();
        List<QuestionWrapper> questionWrapperList = new ArrayList<>();


        for (Question q: questionList) {
            QuestionWrapper qw = new QuestionWrapper(q.getId(),q.getTitle(),q.getOption1(),q.getOption2()
            ,q.getOption3(),q.getOption4());
            questionWrapperList.add(qw);
        }
        return  questionWrapperList;
    }

    public List<Question> getExamQuestionFromData(int id) {
        Optional<Exam> exam = examDAO.findById(id);
        List<Question> questionList = exam.get().getQuestions();
        return  questionList;
    }

    public int caculateResult(int id, List<UserResponse> response) {
        //  Exam exam = examDAO.findById(id).get(); // cach viet khac cau optional
        Optional<Exam> exam = examDAO.findById(id);
        List<Question> questions = exam.get().getQuestions();
        int rightQuantity = 0;
        int i=0;
        for(UserResponse userResponse: response){
            if(userResponse.getSelectedOption().equals(questions.get(i).getRightAnswer())){
                rightQuantity++;
            }
            i++;
        }
        return rightQuantity;
    }
    @Transactional
    public void saveUserResponse(ExamHistory examHistory, List<UserResponse> userResponses){
        for (UserResponse response: userResponses) {
            response.setExamHistory(examHistory);
            userResponseDAO.save(response);
        }

    }

    @Transactional
    public void saveExamResult(ExamHistory result, List<UserResponse> responses) {
        examHistoryDAO.save(result);
        saveUserResponse(result,responses);
    }


    public List<QuestionWrapper> getExamQuestionsForPart(int id, int i, int i1) {
        Optional<Exam> exam = examDAO.findById(id);
        List<Question> questionList = exam.get().getQuestions();
        i=0;
        i1=0;
        List<QuestionWrapper> questionWrapperList = new ArrayList<>();


        for (Question q: questionList) {
            QuestionWrapper qw = new QuestionWrapper(q.getId(),q.getTitle(),q.getOption1(),q.getOption2()
                    ,q.getOption3(),q.getOption4());
            questionWrapperList.add(qw);
        }
        return  questionWrapperList;
    }

}
