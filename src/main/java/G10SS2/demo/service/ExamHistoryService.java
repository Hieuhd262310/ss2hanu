package G10SS2.demo.service;

import G10SS2.demo.dao.ExamHistoryDAO;
import G10SS2.demo.entity.ExamHistory;
import G10SS2.demo.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExamHistoryService {
    @Autowired
    private ExamHistoryDAO examHistoryDAO;

    public ExamHistory getUserExamResultById(int id) {
        return examHistoryDAO.getReferenceById(id);
    }


    public List<ExamHistory> getAllHistory() {
        return examHistoryDAO.findAll();
    }

    public List<ExamHistory> findByUser(User user) {
        return examHistoryDAO.findByUser(user);
    }
}
