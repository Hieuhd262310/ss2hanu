package G10SS2.demo.service;

import G10SS2.demo.dao.RoleDAO;
import G10SS2.demo.dao.UserDAO;
import G10SS2.demo.entity.Role;
import G10SS2.demo.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class CustomOAuth2UserService extends DefaultOAuth2UserService {

    @Autowired
    private UserDAO userRepository;
    @Autowired
    private RoleDAO roleRepository;

    @Transactional
    @Override
    public OAuth2User loadUser(OAuth2UserRequest userRequest) {
        OAuth2User oAuth2User = super.loadUser(userRequest);
        return processOAuth2User(oAuth2User);
    }

    private OAuth2User processOAuth2User(OAuth2User oAuth2User) {
        String email = oAuth2User.getAttribute("email");
        User user = userRepository.findByEmail(email);
        if (user == null) {
            user = new User();
            user.setEmail(email);
            user.setUsername(oAuth2User.getAttribute("name"));
            userRepository.save(user);
        }

        List<Role> roles = new ArrayList<>();
        if (email.equalsIgnoreCase("tsrunghieutranst@gmail.com")) {
            roles.add(getRole("ROLE_ADMIN"));
        } else {
            roles.add(getRole("ROLE_STUDENT"));
        }
        user.setRoles(roles);
        userRepository.save(user);

        return new CustomOAuth2User(oAuth2User.getAttributes(), roles);
    }

    private Role getRole(String roleName) {
        Role role = roleRepository.findByName(roleName);
        if (role == null) {
            role = new Role();
            role.setName(roleName);
            roleRepository.save(role);
        }
        return role;
    }
}

class CustomOAuth2User implements OAuth2User {
    private final Map<String, Object> attributes;
    private final List<SimpleGrantedAuthority> authorities;

    public CustomOAuth2User(Map<String, Object> attributes, List<Role> roles) {
        this.attributes = attributes;
        this.authorities = roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());
    }

    @Override
    public String getName() {
        return String.valueOf(attributes.get("name"));
    }

    @Override
    public Map<String, Object> getAttributes() {
        return attributes;
    }

    @Override
    public List<SimpleGrantedAuthority> getAuthorities() {
        return authorities;
    }
}
