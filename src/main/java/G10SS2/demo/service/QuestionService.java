package G10SS2.demo.service;

import G10SS2.demo.dao.QuestionDAO;
import G10SS2.demo.entity.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionService {
    @Autowired
    private QuestionDAO questionDAO;

    public List<Question> getAllQuestion() {
        return questionDAO.findAll();
    }

    public List<Question> getQuestionByTitle(String title) {
        return questionDAO.findByTitle(title);
    }

    public void saveQuestion(Question question) {
        questionDAO.save(question);
    }


    public Question getQuestionById(int id) {
        return questionDAO.getById(id);
    }

    public void deleteQuestion(int id) {
        questionDAO.deleteById(id);
    }
}
