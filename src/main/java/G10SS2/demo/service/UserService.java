package G10SS2.demo.service;

import G10SS2.demo.dao.UserDAO;
import G10SS2.demo.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {
    @Autowired
    private UserDAO userDAO;
    public User findByEmail(String email) {
      return   userDAO.findByEmail(email);
    }

    public void save(User user) {
        User existUser = userDAO.findByEmail(user.getEmail());
        if(existUser == null){
            userDAO.save(user);
        }
    }

    public Optional<User> findById(int id) {
       return userDAO.findById(id);
    }


}
