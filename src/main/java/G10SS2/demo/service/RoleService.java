package G10SS2.demo.service;

import G10SS2.demo.dao.RoleDAO;
import G10SS2.demo.entity.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


public class RoleService {
    @Autowired
    private RoleDAO roleDAO;

    public Role findByName(String admin) {
        return roleDAO.findByName(admin);

    }
}
