package G10SS2.demo.config;

import G10SS2.demo.service.CustomOAuth2UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class Config {

    @Autowired
    private CustomOAuth2UserService customOAuth2UserService;

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests(authz -> authz
                        .requestMatchers("/login**", "/error**").permitAll()
                        .anyRequest().authenticated()
                )
                .oauth2Login(oauth2 -> oauth2
                        .loginPage("/login")
                        .defaultSuccessUrl("/loginSuccess", true)
                        .userInfoEndpoint(userInfo -> userInfo
                                .userService(customOAuth2UserService)  // Sử dụng CustomOAuth2UserService
                        )
                        .failureUrl("/login?error")
                        .permitAll()
                )
                .logout(logout -> logout
                        .logoutUrl("/logout") // Định nghĩa URL để thực hiện đăng xuất
                        .logoutSuccessUrl("/login?logout") // Định nghĩa URL sau khi đăng xuất thành công
                        .deleteCookies("JSESSIONID") // Xóa cookie JSESSIONID sau khi đăng xuất
                        .invalidateHttpSession(true) // Vô hiệu hóa HTTP session sau khi đăng xuất
                        .permitAll() // Cho phép truy cập vào URL /logout mà không cần xác thực
                );

        return http.build();
    }
}
